package de.connect2x.trixnity.messenger.compose.example

import android.content.Context
import de.connect2x.trixnity.messenger.MessengerConfig

/**
 * Since Android has no single point of entry, configure the messenger everywhere. This includes Services and Activities.
 */
val messengerConfig: (Context?) -> MessengerConfig.() -> Unit = {
    {
        appName = "TrixnityMessengerDemoApp"
        encryptDb = false
        defaultHomeServer = "https://matrix.org"
    }
}