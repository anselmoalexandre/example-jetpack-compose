package de.connect2x.trixnity.messenger.compose.example.connect

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import de.connect2x.trixnity.messenger.compose.example.ui.theme.TrixnityMessengerJetpackComposeExampleTheme
import de.connect2x.trixnity.messenger.viewmodel.connecting.MatrixClientInitializationViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

@Composable
fun MatrixClientInitializationView(matrixClientInitializationViewModel: MatrixClientInitializationViewModel) {
    val currentState = matrixClientInitializationViewModel.currentState.collectAsState().value
    Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Text(text = "Initialization: $currentState")
    }
}

@Preview(showBackground = true)
@Composable
fun MatrixClientInitializationPreview() {
    TrixnityMessengerJetpackComposeExampleTheme {
        MatrixClientInitializationView(object : MatrixClientInitializationViewModel {
            override val currentState: StateFlow<String> = MutableStateFlow("Loading...")
        })
    }
}