package de.connect2x.trixnity.messenger.compose.example.connect

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.connect2x.trixnity.messenger.compose.example.ui.theme.TrixnityMessengerJetpackComposeExampleTheme
import de.connect2x.trixnity.messenger.viewmodel.connecting.PasswordLoginViewModel
import de.connect2x.trixnity.messenger.viewmodel.connecting.PreviewPasswordLoginViewModel
import kotlinx.coroutines.flow.update

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PasswordLoginView(passwordLoginViewModel: PasswordLoginViewModel) {
    val username = passwordLoginViewModel.username.collectAsState().value
    val password = passwordLoginViewModel.password.collectAsState().value
    val canLogin = passwordLoginViewModel.canLogin.collectAsState().value

    Column(
        Modifier
            .fillMaxSize()
            .padding(20.dp)
    ) {
        Text("Login", style = MaterialTheme.typography.headlineLarge)
        Spacer(Modifier.size(20.dp))
        Column(Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center) {
            Text("Server: ${passwordLoginViewModel.serverUrl}")
            Spacer(Modifier.size(20.dp))
            OutlinedTextField(
                value = username,
                onValueChange = { value -> passwordLoginViewModel.username.update { value } },
                label = { Text("username") },
                modifier = Modifier.fillMaxWidth(),
            )
            Spacer(Modifier.size(20.dp))
            OutlinedTextField(
                value = password,
                onValueChange = { value -> passwordLoginViewModel.password.update { value } },
                visualTransformation = PasswordVisualTransformation(),
                label = { Text("password") },
                modifier = Modifier.fillMaxWidth(),
            )
            Spacer(Modifier.size(20.dp))
            Box(Modifier.fillMaxWidth(), contentAlignment = Alignment.CenterEnd) {
                Button(onClick = { passwordLoginViewModel.tryLogin() }, enabled = canLogin) {
                    Text("Login")
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PasswordLoginPreview() {
    TrixnityMessengerJetpackComposeExampleTheme {
        val passwordLoginViewModel = PreviewPasswordLoginViewModel()
        PasswordLoginView(passwordLoginViewModel)
    }
}