package de.connect2x.trixnity.messenger.compose.example

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.Children
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.animation.fade
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.animation.stackAnimation
import com.arkivanov.decompose.router.stack.ChildStack
import com.arkivanov.decompose.value.Value
import de.connect2x.trixnity.messenger.compose.example.roomlist.RoomListView
import de.connect2x.trixnity.messenger.compose.example.sync.SyncView
import de.connect2x.trixnity.messenger.viewmodel.MainViewModel
import de.connect2x.trixnity.messenger.viewmodel.initialsync.InitialSyncRouter.InitialSyncConfig
import de.connect2x.trixnity.messenger.viewmodel.initialsync.InitialSyncRouter.InitialSyncWrapper
import de.connect2x.trixnity.messenger.viewmodel.roomlist.RoomListRouter.RoomListConfig
import de.connect2x.trixnity.messenger.viewmodel.roomlist.RoomListRouter.RoomListWrapper

@Composable
fun MainView(mainViewModel: MainViewModel) {
    Box(Modifier.fillMaxSize()) {
        RoomListSwitch(mainViewModel.roomListRouterStack)
        // the initial sync should be displayed on top of everything else
        InitialSyncSwitch(mainViewModel.initialSyncStack)
    }
}

@Composable
private fun InitialSyncSwitch(initialSyncStack: Value<ChildStack<InitialSyncConfig, InitialSyncWrapper>>) {
    Children(stack = initialSyncStack) {
        when (val child = it.instance) {
            is InitialSyncWrapper.None -> Box {}
            is InitialSyncWrapper.Undefined -> Text("Initial Sync is in an undefined state")
            is InitialSyncWrapper.Sync -> SyncView(child.syncViewModel)
        }
    }
}

@Composable
private fun RoomListSwitch(roomListRouterStack: Value<ChildStack<RoomListConfig, RoomListWrapper>>) {
    Children(stack = roomListRouterStack, animation = stackAnimation(fade())) {
        when (val child = it.instance) {
            is RoomListWrapper.None -> Box {}
            is RoomListWrapper.List -> RoomListView(child.roomListViewModel)
            else -> Text("State of RoomListRouter: ${child::class::qualifiedName}")
        }
    }
}
