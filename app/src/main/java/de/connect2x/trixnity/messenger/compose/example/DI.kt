package de.connect2x.trixnity.messenger.compose.example

import android.content.Context
import de.connect2x.trixnity.messenger.trixnityMessengerModule
import org.koin.core.KoinApplication
import org.koin.core.context.GlobalContext
import org.koin.dsl.module

/**
 * Please ensure that the MessengerConfig is correctly initialised before.
 */
fun startKoin(context: Context): KoinApplication {
    // since the KoinApplication might be started from multiple places (services, activities, ...), init once and add
    // to the global context, then re-use in later attempts
    return GlobalContext.getKoinApplicationOrNull()
        ?: org.koin.core.context.startKoin {
            modules(
                trixnityMessengerModule(),
                module {
                    single<Context> { context }
                },
            )
        }
}