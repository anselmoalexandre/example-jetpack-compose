package de.connect2x.trixnity.messenger.compose.example

import androidx.compose.foundation.layout.Box
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.Children
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.animation.fade
import com.arkivanov.decompose.extensions.compose.jetbrains.stack.animation.stackAnimation
import com.arkivanov.decompose.router.stack.ChildStack
import com.arkivanov.decompose.value.Value
import de.connect2x.trixnity.messenger.compose.example.connect.AddMatrixAccountView
import de.connect2x.trixnity.messenger.compose.example.connect.MatrixClientInitializationView
import de.connect2x.trixnity.messenger.compose.example.connect.PasswordLoginView
import de.connect2x.trixnity.messenger.viewmodel.RootRouter
import de.connect2x.trixnity.messenger.viewmodel.RootViewModel

@Composable
fun RootView(rootViewModel: RootViewModel) {
    RootSwitch(rootViewModel.rootStack)
}

@Composable
private fun RootSwitch(rootStack: Value<ChildStack<RootRouter.Config, RootRouter.RootWrapper>>) {
    // Children is a helper from `com.arkivanov.decompose:extensions-compose-jetbrains`,
    // see https://arkivanov.github.io/Decompose/extensions/compose/#navigating-between-composable-components
    Children(stack = rootStack, animation = stackAnimation(fade())) {
        when(val child = it.instance) {
            is RootRouter.RootWrapper.None -> Box {}
            is RootRouter.RootWrapper.Main -> MainView(child.mainViewModel)
            is RootRouter.RootWrapper.MatrixClientInitialization -> MatrixClientInitializationView(child.matrixClientInitializationViewModel)
            is RootRouter.RootWrapper.AddMatrixAccount -> AddMatrixAccountView(child.addMatrixAccountViewModel)
            is RootRouter.RootWrapper.PasswordLogin -> PasswordLoginView(child.passwordLoginViewModel)
            else -> ImplementMeView(child)
        }
    }
}

@Composable
private fun ImplementMeView(child: RootRouter.RootWrapper) {
    Text("The state ${child::class.qualifiedName} needs to be implemented.")
}