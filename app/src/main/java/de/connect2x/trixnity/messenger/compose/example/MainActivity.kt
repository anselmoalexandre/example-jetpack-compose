package de.connect2x.trixnity.messenger.compose.example

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.arkivanov.decompose.DefaultComponentContext
import com.arkivanov.essenty.lifecycle.LifecycleRegistry
import de.connect2x.trixnity.messenger.MessengerConfig
import de.connect2x.trixnity.messenger.compose.example.ui.theme.TrixnityMessengerJetpackComposeExampleTheme
import de.connect2x.trixnity.messenger.viewmodel.RootViewModelImpl

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // configure the messenger to our needs
        MessengerConfig.instance.apply(messengerConfig(this))
        // start the dependency injection
        val koinApplication = startKoin(this)
        // create the RootViewModel from where we can navigate to all other views
        val rootViewModel = RootViewModelImpl(
            componentContext = DefaultComponentContext(LifecycleRegistry()),
            koinApplication = koinApplication,
        )

        setContent {
            TrixnityMessengerJetpackComposeExampleTheme {
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    RootView(rootViewModel = rootViewModel)
                }
            }
        }
    }
}
